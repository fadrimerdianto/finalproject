package fotokita;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "version")
@XmlAccessorType (XmlAccessType.PUBLIC_MEMBER)
public class Basic {

	private String canvas;
	
	@XmlValue()
	public String getCanvas() {
		return canvas;
	}

	public void setCanvas(String name) {
		this.canvas = name;
	}

	private String title;
	
	@XmlValue()
	public String getTitle() {
		return title;
	}

	public void setTitle(String name) {
		this.title = name;
	}
}