package fotokita;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"author", "title", "canvas", "dateCreated", "lastModified", "pages"})
@XmlRootElement(name = "fotokita")
@XmlAccessorType (XmlAccessType.PROPERTY)
public class XML {
	
//	Canvas
	private Canvas canvas = new Canvas();
	
	public Canvas getCanvas(){
		return canvas;
	}
	
	public void setCanvas(Canvas canvas){
		this.canvas = canvas;
	}

//	Author
	private Author author = new Author();
	
	public Author getAuthor(){
		return author;
	}
	
	public void setAuthor(Author author){
		this.author = author;
	}

//	Version
	Version version = new Version();
	
	@XmlAttribute()
	public Version getVersion() {
		return version;
	}

	public void setVersion(Version version) {
		this.version = version;
	}
	
//	Title
	private Title title = new Title();
	
	public Title getTitle(){
		return title;
	}
	
	public void setTitle(Title title){
		this.title = title;
	}
	
//	Pages
	private Pages pages = new Pages();
	
	public Pages getPages(){
		return pages;
	}
	
	public void setPages(Pages pages){
		this.pages = pages;
	}

//	Date created
	@XmlElement(name="createdat")
	public String getDateCreated(){
//		Date date = now();
		SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd 'at' hh:mm:ss a zzz");
		return dateFormat.format(Calendar.getInstance().getTime());
	}
	
//	Date created
	@XmlElement(name="lastmodified")
	public String getLastModified(){
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd 'at' hh:mm:ss a zzz");
		return dateFormat.format(date);
	}
}