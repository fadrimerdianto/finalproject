package fotokita;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Decorations;

@XmlAccessorType (XmlAccessType.PUBLIC_MEMBER)
public class Paragraph {

	private String text;
	@XmlElement(name="text")
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

//	Font
	private String font;
	
	@XmlAttribute(name = "font")
	public String getFont() {
		return font;
	}

	public void setFont(String font) {
		this.font = font;
	}
	
//	Color
	private String color;
	
	@XmlAttribute(name="color")
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
//	Size
	private String size;
	
	@XmlAttribute(name="size")
	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}
}