package fotokita;

import java.io.IOException;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "image")
@XmlAccessorType (XmlAccessType.PUBLIC_MEMBER)
public class Gambar {

	private String gambar;
	private String encoded = "null";

    public void setImage(String url) {

        String imgstr = null;
        
        //Convert binary image file to String data
        try {
			encoded = Base64.encodeFromFile(url);
//        	System.out.println(encoded);
            this.gambar = encoded;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
        	System.out.println("coba lagi");
		}
//    	return encoded;
    }
    
    @XmlValue()
    public String getImages(){
    	return encoded;
    }
}