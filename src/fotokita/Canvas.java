package fotokita;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "canvas")
@XmlAccessorType (XmlAccessType.PUBLIC_MEMBER)
public class Canvas {

	private String name;
	
	@XmlValue()
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}