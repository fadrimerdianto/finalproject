package fotokita;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.io.File;
import java.util.Date;

public class CreateXML {
	public static void coba(){
		System.out.println("hai");
	}
	 public static void main(String argv[]) {

	      try {
	         DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	         Document doc = dBuilder.newDocument();

	         // root element
	         Element rootElement = doc.createElement("fotokita");
	         doc.appendChild(rootElement);

	         // version element
	         Attr versionAttr = doc.createAttribute("version");
	         versionAttr.setValue("1");
	         rootElement.setAttributeNode(versionAttr);
	         
	         // Title element
	         Element title = doc.createElement("title");
	         rootElement.appendChild(title);

	         // title child
	         title.appendChild(doc.createTextNode("Jaman Kuliah"));
	         
	         // Date element
	         Element date = doc.createElement("date");
	         rootElement.appendChild(date);
	         
	         // set date attribute to date element
	         Attr dateAttr = doc.createAttribute("created");
	         Date datenow = new Date();
	         dateAttr.setValue(datenow.toString());
	         date.setAttributeNode(dateAttr);
	         
	         // Author element
	         Element author = doc.createElement("author");
	         rootElement.appendChild(author);
	         
	         // author child
	         author.appendChild(doc.createTextNode("Fadri"));
	         
	         // Canvas element
	         Element canvas = doc.createElement("canvas");
	         rootElement.appendChild(canvas);
	         
	         String iter = "";
	         for(int i=1; i <= 5; i++)
	         {
	        	 // Page element as a child of canvas
	        	 Element page = doc.createElement("page");
	        	 canvas.appendChild(page);

	        	 // set page attribute
	        	 Attr pageAttr = doc.createAttribute("no");
	        	 iter = Integer.toString(i);
		         pageAttr.setValue(iter);
		         page.setAttributeNode(pageAttr);
	        	 
		         // Object element as a child of canvas
		         Element object = doc.createElement("object");
		         page.appendChild(object);

		         // Frame element as a child of object
		         Element frame = doc.createElement("frame");
		         object.appendChild(frame);
		         
		         // frame child
		         frame.appendChild(doc.createTextNode("bunga"));

		         // Obj_attr element as a child of object
		         Element obj_attr = doc.createElement("attribute");
		         object.appendChild(obj_attr);

		         // Obj_type element as a child of object
		         Element obj_type = doc.createElement("type");
		         object.appendChild(obj_type);
		         
		         // Obj_type child
		         obj_type.appendChild(doc.createTextNode("text"));
	         }
	         
	         // write the content into xml file
	         TransformerFactory transformerFactory = TransformerFactory.newInstance();
	         Transformer transformer = transformerFactory.newTransformer();
	         DOMSource source = new DOMSource(doc);
	         StreamResult result = new StreamResult(new File("/Project/COBATA/fotokita.fbk"));
	         transformer.transform(source, result);
	         
	         // Output to console for testing
	         StreamResult consoleResult = new StreamResult(System.out);
	         transformer.transform(source, consoleResult);
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	 }
}