package fotokita;

import java.io.File;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;

import org.eclipse.swt.*;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.*;

public class CreateXMLDemo {
	static XML xml = new XML();
	private static void initUI(Display display){
		
//		Allocation and initialization
        Shell shell = new Shell(display, SWT.SHELL_TRIM | SWT.CENTER);
        
//      create layout
        GridLayout layout = new GridLayout(2, false);
        shell.setLayout(layout);
        shell.setText("Fotokita");

//      version
        Label l_version = new Label(shell, SWT.NONE);
        l_version.setText("version");
        final Text tb_version = new Text(shell, SWT.NONE);

//      title
        Label l_title = new Label(shell, SWT.NONE);
        l_title.setText("title");
        final Text tb_title = new Text(shell, SWT.NONE);

//      author
        Label l_author = new Label(shell, SWT.NONE);
        l_author.setText("author");
        final Text tb_author = new Text(shell, SWT.NONE);
        
//      page size
        Label l_pagesize = new Label(shell, SWT.NONE);
        l_pagesize.setText("page size");
        final Text tb_pagesize = new Text(shell, SWT.NONE);
        
//      canvas
        Label l_canvas = new Label(shell, SWT.NONE);
        l_canvas.setText("canvas");
        final Text tb_canvas = new Text(shell, SWT.NONE);
        
//      Button OK
        Button btn_ok = new Button(shell, SWT.PUSH);
        btn_ok.setText("OK");
        btn_ok.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				String titleName = tb_title.getText();
				String canvasName = tb_canvas.getText();
				String authorName = tb_author.getText();
				String versionName = tb_version.getText();
				String pageSize = tb_pagesize.getText();
				xml.getCanvas().setName(canvasName);
				xml.getAuthor().setName(authorName);
				xml.getTitle().setName(titleName);
				xml.getVersion().setName(versionName);
				Pages pages = new Pages();
				pages.setPages(new ArrayList<Page>());
				pages.setSize(pageSize);
				Page page1 = new Page();
				page1.setPage("1");
				Page page2 = new Page();
				page2.setPage("2");
				xml.getDateCreated();
				xml.getLastModified();
				page1.setObject(new ArrayList<Object>());
				Object obj1 = new Object();
				obj1.setName("objek 1");
				obj1.getImage().setImage("/project/cobata/haha.jpg");
				page2.setObject(new ArrayList<Object>());
				Object obj2 = new Object();
				obj2.setName("objek 2");
				Object obj3 = new Object();
				obj3.setName("objek 3");
				page1.setText(new ArrayList<Paragraph>());
				Paragraph text1 = new Paragraph();
				text1.setText("Aku ingin terbang tinggi");
				text1.setColor("biru");
				text1.setFont("Times New Roman");
				text1.setSize("12");
				page1.setLayout("Layout 1");;
				page1.getObject().add(obj1);
				page1.getObject().add(obj3);
				page2.getObject().add(obj2);
				page1.getText().add(text1);
				pages.getPages().add(page1);
				pages.getPages().add(page2);
				xml.setPages(pages);
				try {
					marshalling();
				} catch (JAXBException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			private Object setImages() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
        	
        });
        
		shell.pack();
		shell.open();
		while(!shell.isDisposed())
		{
			if(!display.readAndDispatch())
			{
				display.sleep();
			}
		}
	}
	public static void marshalling() throws JAXBException {

		File file = new File("/Project/COBATA/fotokita3.fbk");
		JAXBContext jaxbContext = JAXBContext.newInstance(XML.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//        jaxbMarshaller.setProperty("eclipselink.media-type", "application/json");
		jaxbMarshaller.marshal(xml, file);
		jaxbMarshaller.marshal(xml, System.out);
	}
	public static void main(String[] args) throws JAXBException{
		Display display = new Display();
		initUI(display);
		display.dispose();
//		marshalling();
	}
}
