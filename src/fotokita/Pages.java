package fotokita;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType (XmlAccessType.PUBLIC_MEMBER)
public class Pages {

	@XmlElement(name="page")
    private List<Page> halaman = null;
	private String pageSize;
    protected List<Page> getPages() {
        return halaman;
    }
 
    public void setPages(List<Page> pages) {
        this.halaman = pages;
    }
    @XmlAttribute(name= "size")
	public String getSize() {
		return pageSize;
	}

	public void setSize(String pageSize) {
		this.pageSize = pageSize;
	}
}