package fotokita;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Decorations;

@XmlAccessorType (XmlAccessType.PUBLIC_MEMBER)
public class Object {

	private String object;
	@XmlAttribute(name = "name")
	public String getName() {
		return object;
	}

	public void setName(String object) {
		this.object = object;
	}

    private double x, y, w, h;
    @XmlAttribute(name= "x")
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

    @XmlAttribute(name= "y")
	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
    @XmlAttribute(name= "w")
	public double getW() {
		return w;
	}

	public void setW(double w) {
		this.w = w;
	}

    @XmlAttribute(name= "h")
	public double getH() {
		return h;
	}

	public void setH(double h) {
		this.h = h;
	}
	
	private Gambar image = new Gambar();

	public Gambar getImage(){
		return image;
	}
	
	public void setImage(Gambar image){
		this.image = image;
	}
}