package fotokita;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType (XmlAccessType.PUBLIC_MEMBER)
public class Page {

	private String page;

	@XmlAttribute(name= "no")
	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	@XmlElement(name="object")
    private List<Object> object = null;

    protected List<Object> getObject() {
        return object;
    }
 
    public void setObject(List<Object> object) {
        this.object = object;
    }
    
    private List<Paragraph> text = null;

	@XmlElement(name="text")
    protected List<Paragraph> getText() {
        return text;
    }
 
    public void setText(List<Paragraph> text) {
        this.text = text;
    }

    private String layout;

    public String getLayout(){
    	return layout;
    }
    
    public void setLayout(String layout){
    	this.layout = layout;
    }
}